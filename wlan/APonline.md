# AP简易上线流程
## 基础网络配置部分
- vlan `VLANID`
	- 创建VLAN
- dhcp enable
	- 开启DHCP服务
- interface vlanif `VLANID`
	- 创建VLANIF子接口
- ip address `IPAddress` `NETMASK`
	- 为VLANIF子接口添加IP地址
- dhcp select interface
	- 开启接口下DHCP地址池
- 将连接AP的接口VLAN划分完成

## CAPWAP配置
- capwap source interface vlanif `VLANID`
	- Control And Provisioning of Wireless Access Points Protocol Specification
	- 通用隧道协议 负责完成AP发现AC等基本协议功能

## WLAN视图下配置
- wlan 
	- 进入WLAN视图下
- ap-confirm all
	- AP上线指令, 并使用`display ap all`来查看AP是否全部上线
	- AP全部上线后再继续其他配置

### 配置监管域模板
- regulatory-domain-profile name `ProfileName`
	- 添加监管域模板
- country-code `CountryCode`
	- 因无线设备需要向多个国家销售, 因此需要添加不同的监管域模板来顺从地区法规
	- 中国国家代码为cn

### AP组配置
- ap-group name `GroupName`
	- 设定AP组名
- regulatory-domain-profile `ProfileName`
	- 在AP组设定中指定监管域模板信息

### SSID模板
- ssid-profile name `ProfileName`
	- 配置SSID模板信息
- ssid `SSID`
	- 指定SSID信息

### Security安全模板
- security-profile name `ProfileName`
	- 创建安全模板
- security `open | wapi | wep | wpa | wpa-wpa2 | wpa2` `dot1x | psk` `pass-phrase | hex` `Password` `aes | aes-tkip | tkip`
	- 设定安全加密的方式与协议 密钥类型 10或16进制 密码 加密算法
	- 如: security wpa2 psk pass-phrase Huawei123 aes

### VAP配置
- Virtual Access Point 虚拟接入点
- vap-profile name `ProfileName`
	- 配置VAP配置文件
- ssid-profile `ProfileName`
	- 在VAP模板中指定SSID模板
- security-profile `ProfileName`
	- 在VAP模板中指定加密模板
- forward-mode tunnel
	- 指定转发方式为隧道
- service-vlan vlan-id `VLANID`
	- 指定使用的VLANID

### 应用AP组与频段
- ap-confirm all
	- 首先将之前的配置下发
- ap-id 0
	- 通过APID配置AP
- ap-group name `APGroupName`
	- 将该AP划分到AP组中
- 将所有AP划分到AP组后并进入该AP组
- radio `RADIO ID`
	- 选择该AP组的AP频段
- vap-profile `ProfileName` wlan `WlanID1-16`






